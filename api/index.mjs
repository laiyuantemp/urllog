import express from 'express'
import mysql from 'mysql2/promise'


const app = express()
const DB_PW_URI = process.env.DB_PW_URI
const API_KEY = process.env.API_KEY

class DB{
    async start_db(){
        this.pool = mysql.createPool(DB_PW_URI)
        this.connection = await this.pool.getConnection()

        var [results, fields] = await this.connection.query(
            'CREATE TABLE IF NOT EXISTS `kv-table` ( \
                `key` VARCHAR(200) NOT NULL, \
                `value` MEDIUMTEXT, \
                PRIMARY KEY (`key`) \
            )'
        )
    }
    async end_db(){
        await this.connection.release()
        await this.pool.end()
    }
    //this.kv
    async pull_cache(force=false){
        if(this.kv && !force)
            return

        try{
            var [results, fields] = await this.connection.query(
                "SELECT * FROM `kv-table` WHERE `key`='urllog-kv'"
            )
            this.kv = JSON.parse(results[0]['value']) || []
        }catch(e){
            console.log(e)
            this.kv = []
        }
    }
    async push_cache(){
        var [results, fields] = await this.connection.query(
            {sql:"REPLACE INTO `kv-table` (`key`,`value`) VALUES('urllog-kv',?)"},
            [JSON.stringify(this.kv)]
        )
    }
    async purge_cache(){
        var [results, fields] = await this.connection.query(
            "DELETE FROM `kv-table` WHERE `key`='urllog-kv'"
        )
        this.kv = []
    }
}

const db = new DB()

function kv_to_json(kv){
    return kv.map((e,i)=>(
        `${i}: ${e}`
    )).join('\n')
}

app.use(express.urlencoded({extended: true}))
app.use(/.*/, (req, res, next)=>{
    if(req.headers['authorization'] !== API_KEY){
        res.status(401).send('unauthorized')
        return
    }
    next()
})

app.get('/get', async (req, res)=>{
    await db.start_db()
    await db.pull_cache()
    await db.end_db()
    res.send(kv_to_json(db.kv))
})

app.post('/put', async (req, res)=>{
    await db.start_db()
    await db.pull_cache()
    if(req.body.f){
        db.kv = db.kv.concat(req.body.f)
        await db.push_cache()
    }
    await db.end_db()
    res.send(kv_to_json(db.kv))
})

app.get('/delete', async (req, res)=>{
    await db.start_db()
    await db.pull_cache()
    var n_arr = req.query.n.split(' ').map(
        n=>Number.parseInt(n)
    ).filter(
        n=>!Number.isNaN(n)
    ).sort((a,b)=>(a-b)).reverse()
    n_arr = [... new Set(n_arr)]
    n_arr.forEach(n=>
        db.kv.splice(n,1)
    )
    await db.push_cache()
    await db.end_db()
    res.send(kv_to_json(db.kv))
})

app.post('/patch', async (req, res)=>{
    await db.start_db()
    await db.pull_cache()
    db.kv[req.body.n] = req.body.f
    await db.push_cache()
    await db.end_db()
    res.send(kv_to_json(db.kv))
})

app.get('/deleteAll', async (req, res)=>{
    await db.start_db()
    await db.purge_cache()
    await db.push_cache()
    await db.end_db()
    res.send(kv_to_json(db.kv))
})

app.get('/json', async (req, res)=>{
    await db.start_db()
    await db.pull_cache()
    await db.end_db()
    res.set('Content-disposition', 'attachment; filename=urllog.json')
    res.json(db.kv)
})

app.listen(process.env.PORT || 8080, () => console.log('Server ready'))